package com.vu.assignment.dys.domain;

import java.util.Random;
import java.util.UUID;

public abstract class Participant implements BattleParticipant {
    public static Integer BATTLEFIELDSIZE = 25;
    private int tId = Math.abs(UUID.randomUUID().hashCode());
    int initialHp, hp,ap, x, y;

    public Participant() { spawn(); }
    public Participant(int id) {tId=id;}
    public Participant(int x, int y) { this.x=x; this.y=y;}
    public Participant(int x, int y, int hp, int ap, int initialHp) {
        this(x,y);
        this.hp=hp; this.ap=ap;
        this.initialHp=initialHp;
    }
    public Participant(int id, int x, int y, int hp, int ap, int initialHp) {
        this(x,y,hp,ap,initialHp);
        this.tId=id;
    }

    public boolean setHp(int hp) {
        this.hp=hp;
        return true;
    }

    public int getHp() {
        return hp;
    }

    public boolean setAp(int ap) {
        this.ap=ap;
        return true;
    }

    public int getAp() {
        return ap;
    }

    @Override
    public int getInitialHp() {
        return initialHp;
    }

    @Override
    public boolean equals(Object obj) {
        return ((BattleParticipant) obj).hashCode()==hashCode();
    }

    @Override
    public int hashCode() {
        return tId;
    }

    @Override
    public int getPlayerId() {
        return tId;
    }

    @Override
    public void setXPos(int x) {
        this.x=x;
    }

    @Override
    public void setYPos(int y) {
        this.y=y;
    }

    @Override
    public int getXPos() {
        return x;
    }

    @Override
    public int getYPos() {
        return y;
    }

    @Override
    public void spawn() {
        x = new Random().nextInt(BATTLEFIELDSIZE);
        y = new Random().nextInt(BATTLEFIELDSIZE);
    }

    @Override
    public String toString() {
        return String.format("Player id: %s, x:%s,y:%s, hp:%s,ap:%s",
                tId,x,y,hp,ap);
    }
}

