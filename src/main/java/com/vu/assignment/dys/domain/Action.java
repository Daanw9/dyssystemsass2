package com.vu.assignment.dys.domain;

import java.util.UUID;

public enum Action {
    CONNECT,DISCONNECT,UPDATE,SPAWNDRAGON,LEFT,RIGHT,UP,DOWN,STRIKE,HEAL,SHUTDOWNNODE,ADDNODE,GETSTATS;
}
