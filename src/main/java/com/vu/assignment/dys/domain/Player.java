package com.vu.assignment.dys.domain;

import java.util.Random;

public class Player extends Participant {
    public Player() {
        super();
        setHp(initialHp = new Random().nextInt(10)+10);
        setAp(new Random().nextInt(10)+1);
    }
    public Player(int id) { super(id); }
    public Player(int x, int y) { super(x,y); }
    public Player(int x, int y, int hp, int ap, int initialHp) {
        super(x,y,hp,ap,initialHp);
    }
    public Player(int id,int x, int y, int hp, int ap, int initialHp) { super(id,x,y,hp,ap, initialHp); }
}
