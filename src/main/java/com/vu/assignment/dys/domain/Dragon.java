package com.vu.assignment.dys.domain;

import java.util.Random;

public class Dragon extends Participant {
    public Dragon() {
        super();
        setHp(initialHp = new Random().nextInt(50)+50);
        setAp(new Random().nextInt(15)+5);
    }
    public Dragon(int x, int y) { super(x,y); }
    public Dragon(int x, int y, int hp, int ap, int initialHp) {
        super(x,y,hp,ap,initialHp);
    }
    public Dragon(int id,int x, int y, int hp, int ap, int initialHp) { super(id,x,y,hp,ap,initialHp); }

    @Override
    public int getPlayerId() {
        return Integer.valueOf(String.valueOf(super.getPlayerId()).replaceAll("^[0-9]{5}","11111"));

    }
}
