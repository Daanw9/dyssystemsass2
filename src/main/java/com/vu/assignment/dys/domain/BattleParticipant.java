package com.vu.assignment.dys.domain;

import java.util.Random;
import java.util.UUID;

public interface BattleParticipant {
    int getPlayerId();
    void setXPos(int x);
    void setYPos(int y);
    int getXPos();
    int getYPos();
    int getInitialHp();
    boolean setHp(int hp); int getHp();
    boolean setAp(int ap); int getAp();
    void spawn();
}