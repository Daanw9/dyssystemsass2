package com.vu.assignment.dys.cluster;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class DistributedMap<K,V> implements CMap<K,V> {
    Set<CActionListener<K,V>> onUpdateListeners, onRemoveListeners, onAddListeners;

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        return null;
    }

    @Override
    public V put(K key, V value) {
        onAddListeners.forEach(s -> s.onAction(key, value));
        return null;
    }

    @Override
    public V remove(Object key) {
        onRemoveListeners.forEach(s -> s.onAction((K)key, null));
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    @Override
    public void addOnRemoveListener(OnRemoveListener removeListener) {

    }

    @Override
    public void addOnAddListener(OnAddListener addListener) {

    }

    @Override
    public void addOnUpdateListener(OnUpdateListener onUpdateListener) {

    }
}
