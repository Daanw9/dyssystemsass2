package com.vu.assignment.dys.cluster;

import com.hazelcast.config.Config;
import com.hazelcast.core.*;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.MapListener;
import com.hazelcast.monitor.LocalMapStats;
import com.vu.assignment.dys.shared.Utility;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;
import java.util.logging.Logger;

import static com.vu.assignment.dys.domain.Action.*;

public class Cluster  {
    List<HazelcastInstance> hzInstances = new ArrayList<>();
    IMap players;
    IQueue<String> tests;
    IQueue<Map<String, Map<String,Long>>> statistics;

    public static Logger logger = Logger.getLogger(Cluster.class.getName());
    public static FileHandler fh;

    Cluster(int instances) {
        Config config = new Config();
        config.getGroupConfig().setName("server-cluster").setPassword("dyssystems");
        Cluster.startLogging();
        for (int i = 0; i < instances; i++) {
            HazelcastInstance e = Hazelcast.newHazelcastInstance(config);
            hzInstances.add(e);
            logger.info("Adding node: " + String.valueOf(i) + " to the cluster.");
            if (players==null) {
                players=e.getMap("players");
                tests = e.getQueue("tests");
                statistics = e.getQueue("statistics");
                tests.addItemListener(new ItemListener<String>() {
                    @Override
                    public void itemAdded(ItemEvent<String> itemEvent) {
                        if (itemEvent.getItem().equals("shutdownnode")) {
                            int index = new Random().nextInt(hzInstances.size());
                            logger.info("Requests for shutdown of node, shutting down node: " + String.valueOf(index));
                            hzInstances.get(index).shutdown();
                            hzInstances.remove(index);
                        } else if (itemEvent.getItem().equals("addnode")) {
                            hzInstances.add(Hazelcast.newHazelcastInstance(config));
                            logger.info("Added new node to cluster.");
                        } else if (itemEvent.getItem().equals("getstats")) {
                            logger.info("Processing stat request.");
                            statistics.offer(getClusterStatistics());
                        }
                    }

                    @Override
                    public void itemRemoved(ItemEvent<String> itemEvent) { }
                },true);
            }
        }
    }

    public Map<String, Map<String,Long>> getClusterStatistics ()
    {
        Map<String, Map<String,Long>> stats = new HashMap<>();
        for (HazelcastInstance inst : Hazelcast.getAllHazelcastInstances()) {
            HashMap<String, Long> statistics = new HashMap<>();
            LocalMapStats localMapStats = inst.getMap("players").getLocalMapStats();

            statistics.put("PUT operation Count", Long.valueOf(localMapStats.getPutOperationCount()));
            statistics.put("Total PUT latency (ms)",Long.valueOf(localMapStats.getTotalPutLatency()));
            statistics.put("Max PUT latency (ms)", Long.valueOf(localMapStats.getMaxPutLatency()));

            long putOps = Long.valueOf(localMapStats.getPutOperationCount());
            if (putOps != 0)
                statistics.put("Average PUT latency (ms)", Long.valueOf(localMapStats.getTotalPutLatency())/ Long.valueOf(localMapStats.getPutOperationCount()));

            statistics.put("GET operation Count", Long.valueOf(localMapStats.getGetOperationCount()));
            statistics.put("Total GET latency (ms)", Long.valueOf(localMapStats.getTotalGetLatency()));
            statistics.put("Max GET latency (ms)", Long.valueOf(localMapStats.getMaxGetLatency()));
            long getOps = localMapStats.getTotalGetLatency();
            if (getOps != 0)
                statistics.put("Average GET latency (ms)", localMapStats.getTotalGetLatency()/ localMapStats.getGetOperationCount());



            // Entries and memory
            statistics.put("Backup Entries on this Node", Long.valueOf(localMapStats.getBackupEntryCount()));
            statistics.put("Memory Cost--Backup (bytes)", Long.valueOf(localMapStats.getBackupEntryMemoryCost()));
            statistics.put("Owned Entries on this Node", Long.valueOf(localMapStats.getOwnedEntryCount()));
            statistics.put("Memory Cost--Owned (bytes)", Long.valueOf(localMapStats.getOwnedEntryMemoryCost()));


            stats.put(inst.getName(), statistics);
        }
        return stats;
    }

    public static void main(String[] args) {
        int numberOfInstances = args.length==1 ? Integer.valueOf(args[0]) : 5;
        Cluster cluster = new Cluster(numberOfInstances);
        logger.info("Cluster is up and running with " + String.valueOf(numberOfInstances) + " nodes.");
    }

    public static void startLogging() {
        try {
            fh = new FileHandler(System.getProperty("user.dir") + "/clusterlogfile.log");
            logger.addHandler(fh);
            fh.setFormatter(new SimpleFormatter());
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}