package com.vu.assignment.dys.cluster;

import java.rmi.Remote;
import java.util.Map;

public interface CMap<K,V> extends Map<K,V>, Remote {
    interface CActionListener<K,V> {
        void onAction(K key, V value);
    }
    interface OnRemoveListener<K,V> extends CActionListener<K,V> {}
    interface OnAddListener<K,V> extends CActionListener<K,V> {}
    interface OnUpdateListener<K,V> extends CActionListener<K,V> {}

    void addOnRemoveListener(OnRemoveListener removeListener);
    void addOnAddListener(OnAddListener addListener);
    void addOnUpdateListener(OnUpdateListener onUpdateListener);
}
