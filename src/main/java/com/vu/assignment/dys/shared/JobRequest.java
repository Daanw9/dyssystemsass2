package com.vu.assignment.dys.shared;

import java.io.Serializable;

public interface JobRequest extends Serializable{
    void execute();
}
