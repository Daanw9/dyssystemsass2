package com.vu.assignment.dys.shared;

import com.vu.assignment.dys.domain.Action;

import java.io.Serializable;
import java.util.UUID;

public interface Command extends Serializable {
    Action getAction();
    int getCommandId();
    int getPlayerId();

    /**
     * getToPlayerId is set when an action is performed which
     * concerns another player, e.g. in case of a strike or a heal.
     * @return the ID of the other player.
     */
    int getToPlayerId();
    int getXPos();
    int getYPos();
    int getHp();
    int getAp();
}
