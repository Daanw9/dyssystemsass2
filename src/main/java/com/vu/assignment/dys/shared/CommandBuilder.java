package com.vu.assignment.dys.shared;

import com.vu.assignment.dys.domain.Action;
import com.vu.assignment.dys.shared.Utility.*;
import java.util.UUID;

public final class CommandBuilder {
    public static final Command getInstance(Action action,
                                            Tuple<Integer,Integer> playerIds,
                                            Tuple<Integer,Integer> coords,
                                            Tuple<Integer,Integer> hpap) {
        return new Command() {
            @Override
            public Action getAction() {
                return action;
            }

            @Override
            public int getCommandId() {
                return Math.abs(UUID.randomUUID().hashCode());
            }

            @Override
            public int getPlayerId() {
                return playerIds.a;
            }

            @Override
            public int getToPlayerId() { return playerIds.b; }

            @Override
            public int getXPos() {
                return coords.a;
            }

            @Override
            public int getYPos() {
                return coords.b;
            }

            @Override
            public int getHp() {
                return hpap.a;
            }

            @Override
            public int getAp() {
                return hpap.b;
            }
        };
    }
}