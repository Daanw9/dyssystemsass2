package com.vu.assignment.dys.shared;

import com.vu.assignment.dys.domain.Action;
import com.vu.assignment.dys.domain.BattleParticipant;
import com.vu.assignment.dys.domain.Player;

import java.io.Serializable;
import java.util.UUID;

public final class Utility {
    private Utility() {}
    public static class Tuple<A, B> implements Serializable {
        public A a;
        public B b;
        Tuple() {}
        Tuple(A a, B b) { this.a = a; this.b= b;}
        A getA() { return a; } B getB() { return b; }

        @Override
        public boolean equals(Object obj) {
            return a.equals(((Tuple)obj).a);
        }

        @Override
        public int hashCode() {
            return a.hashCode();
        }
    }
    public static class Tuple2 extends Tuple<Integer,Integer> {
        Tuple2(int a, int b) { super(a,b); }

        @Override
        public boolean equals(Object obj) {
            Tuple<Integer,Integer> t = (Tuple2) obj;
            return a==t.a && b==t.b;
        }
        @Override
        public String toString() {
            return String.format("(%s,%s)", a,b);
        }
        @Override
        public int hashCode() {
            int hash = 1;
            hash = hash * 17 + a;
            hash = hash * 31 + b;
            return Math.abs(hash);
        }
    }
    public static class Tuple3<A,B,C> implements Serializable {
        public A a;
        public B b;
        public C c;
        Tuple3(A a, B b, C c) { this.a = a; this.b=b; this.c=c; }
        @Override
        public boolean equals(Object obj) {
            return a.equals(((Tuple3)obj).a);
        }
        @Override
        public int hashCode() {
            return a.hashCode();
        }
    }

    public static<A,B> Tuple tuple(A a, B b) {
        return new Tuple<A,B>(a,b);
    }
    public static Tuple2 tuple2(int a,int b) { return new Tuple2(a,b); }
    public static<A,B,C> Tuple3 tuple3(A a, B b, C c) { return new Tuple3<A,B,C>(a,b,c); }

    public static Tuple2 direction(Action a, BattleParticipant p) {
        switch (a) {
            case RIGHT: return tuple2(p.getXPos()+1,p.getYPos());
            case LEFT: return tuple2(p.getXPos()-1,p.getYPos());
            case UP: return tuple2(p.getXPos(),p.getYPos()+1);
            case DOWN: return tuple2(p.getXPos(),p.getYPos()-1);
            default: return null;
        }
    }
    public static Action determineMoveAction(Tuple2 oldt, Tuple2 newt) {
        if (oldt.a < newt.a) return Action.RIGHT;
        else if (oldt.a > newt.a) return Action.LEFT;
        else if (oldt.b < newt.b) return Action.UP;
        else return Action.DOWN;
    }
}
