package com.vu.assignment.dys.client;

import com.vu.assignment.dys.domain.Action;
import com.vu.assignment.dys.domain.BattleParticipant;

import java.util.Map;

public interface IActionListener {
    void perform(Action action, BattleParticipant participant);
}


interface StatsListener extends IActionListener {
    void onStatisticsUpdate(Map<String, Map<String,Long>> item);
}

