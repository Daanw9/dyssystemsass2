package com.vu.assignment.dys.client;

import com.vu.assignment.dys.domain.Action;
import com.vu.assignment.dys.domain.Participant;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import static com.vu.assignment.dys.domain.Action.*;

public class BotClient {
    public static void main(String[] args) {
        int amountOfBots=100;
        if (args.length > 0) {
            Client.publicAddress=args[0];
            Client.publicPort=Integer.valueOf(args[1]);
            Participant.BATTLEFIELDSIZE = Integer.valueOf(args[2]);
            amountOfBots = Integer.valueOf(args[3]);
        }
        for (int i =0; i<amountOfBots;i++) {
            Client c = new Client(true);
            System.out.println("Player id : " + c.player.getPlayerId());
            c.performAction(CONNECT);

            Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    Action[] actions = {UP, DOWN, LEFT, RIGHT};
                    c.performAction(actions[new Random().nextInt(4)]);
                }
            }, 3, 3, TimeUnit.SECONDS);
        }
    }

}
