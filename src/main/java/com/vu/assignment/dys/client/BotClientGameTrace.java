package com.vu.assignment.dys.client;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.vu.assignment.dys.domain.Action;
import com.vu.assignment.dys.domain.BattleParticipant;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;

import static com.vu.assignment.dys.domain.Action.*;

public class BotClientGameTrace {
    public static void main(String[] args) {
        BufferedReader reader = null;


        File GameTrace = new File (System.getProperty("user.dir") + "/WoWSession/WoWSession_Node_Player_Fixed_Dynamic");

        Map<Integer,Client> participants = new HashMap<Integer,Client>();

        try {

            reader = new BufferedReader(new FileReader(GameTrace));
            String line = null;
            while ((line = reader.readLine()) != null) {
                if(line.contains("PLAYER_LOGIN"))
                {
                    Client c = new Client(true);
                    System.out.println("Player id : " + c.player.getPlayerId());
                    c.performAction(CONNECT);


                    c.addListener(new IActionListener() {
                        @Override
                        public void perform(Action action, BattleParticipant participant) {
                            switch (action) {
                                case CONNECT:
                                    participants.put(participant.getPlayerId(),c);
                                    break;

                                case DISCONNECT:
                                    participants.remove(participant.getPlayerId());

                                    break;
                                default: break;
                            }
                        }
                    });

                    Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
                        @Override
                        public void run() {
                            Action[] actions = {UP, DOWN, LEFT, RIGHT};
                            c.performAction(actions[new Random().nextInt(4)]);
                        }
                    }, 3, 3, TimeUnit.SECONDS);


                }

                else if(line.contains("PLAYER_LOGOUT")|| line.contains("PLAYER_QUIT"))
                {
                    if(participants.size() > 0 ) {
                        Map.Entry<Integer, Client> aPart = participants.entrySet().iterator().next();
                        aPart.getValue().performAction(DISCONNECT);
                    }
                }


            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {}
        }



    }

}