package com.vu.assignment.dys.client;

import com.vu.assignment.dys.domain.Action;
import com.vu.assignment.dys.domain.BattleParticipant;
import com.vu.assignment.dys.domain.Participant;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.vu.assignment.dys.domain.Action.CONNECT;
import static com.vu.assignment.dys.domain.Participant.BATTLEFIELDSIZE;
import static com.vu.assignment.dys.shared.Utility.*;

public class InterClient extends Application {
    static Map<Integer,Tuple<Circle,BattleParticipant>> participants = new HashMap<>();
    Set<BattleParticipant> dragons;
    List<IActionListener> listeners;
    public GridPane root;
    Client c;
    ContextMenu contextMenu;
    MenuItem miAttack,miHeal;
    EventHandler getMenuHandler(Circle circle, BattleParticipant p) {
        return new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent event) {
                contextMenu.show(circle,event.getScreenX(),event.getScreenY());
                miAttack.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println(String.format("Attacking player %s",p.getPlayerId()));
                        c.performAction(Action.STRIKE,p.getPlayerId());
                    }
                });
                miHeal.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println(String.format("Healing player %s",p.getPlayerId()));
                        c.performAction(Action.HEAL, p.getPlayerId());
                    }
                });
            }
        };
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        c.performAction(Action.DISCONNECT);
    }

    private void initClient(GridPane root) {
        c = new Client(true);
        System.out.println("Player id : " + c.player.getPlayerId());
        c.performAction(CONNECT);

        c.addListener(new IActionListener() {
            @Override
            public void perform(Action action, BattleParticipant participant) {
                switch (action) {
                    case SPAWNDRAGON:
                    case CONNECT:
                        Platform.runLater(new Runnable() {
                            @Override public void run() {
                                Circle circle = new Circle();
                                circle.setFill(Paint.valueOf(action.equals(CONNECT) ? "grey" : "red"));
                                circle.setRadius(BATTLEFIELDSIZE <= 25 ? 8 : 3);
                                participants.put(participant.getPlayerId(),tuple(circle,participant));
                                root.add(circle, participant.getXPos(), BATTLEFIELDSIZE-1-participant.getYPos());

                                circle.setOnContextMenuRequested(getMenuHandler(circle,participant));
                                setTooltip(participant,circle);
                            }
                        });
                        break;
                    case UPDATE:
                        Platform.runLater(new Runnable() {
                            @Override public void run() {
                                Tuple<Circle, BattleParticipant> pOld = participants.get(participant.getPlayerId());
                                Circle c = pOld.a;
                                Tuple2 oldt = tuple2(pOld.b.getXPos(), pOld.b.getYPos());
                                Tuple2 newt = tuple2(participant.getXPos(), participant.getYPos());
                                if (!oldt.equals(newt)) { /* checking if the player moved */
                                    root.getChildren().remove(pOld.a);
                                    root.add(c,newt.a,BATTLEFIELDSIZE-1-newt.b);
                                }
                                participants.put(participant.getPlayerId(),tuple(c,participant));
                                setTooltip(participant,c);
                            }
                        });
                        break;
                    case DISCONNECT:
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                root.getChildren().remove(participants.get(participant.getPlayerId()).a);
                                participants.remove(participant.getPlayerId());
                            }
                        });
                        break;
                }
            }
        });
    }

    @Override
    public void start(Stage primaryStage) {
        List<String> args = getParameters().getRaw();
        if (args.size()==3) {
            Client.publicAddress=args.get(0);
            Client.publicPort=Integer.valueOf(args.get(1));
            Participant.BATTLEFIELDSIZE=Integer.valueOf(args.get(2));
        }
        root = new GridPane();
        final int size = BATTLEFIELDSIZE;
        contextMenu = new ContextMenu();
        miAttack = new MenuItem("Attack player");
        miHeal = new MenuItem("Heal player");
        contextMenu.getItems().addAll(miAttack,miHeal);
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col ++) {
                StackPane square = new StackPane();
                //square.setLayoutX(35); square.setLayoutY(35);
                int dimensions = BATTLEFIELDSIZE <= 25 ? 25 : 8;
                square.setPrefWidth(dimensions); square.setPrefHeight(dimensions);
                square.setStyle("-fx-background-color: white; -fx-border-color: darkgrey;");
                root.add(square, col, BATTLEFIELDSIZE-1-row);
            }
        }
        for (int i = 0; i < size; i++) {
            root.getColumnConstraints().add(new ColumnConstraints(5, Control.USE_COMPUTED_SIZE, Double.POSITIVE_INFINITY, Priority.ALWAYS, HPos.CENTER, true));
            root.getRowConstraints().add(new RowConstraints(5, Control.USE_COMPUTED_SIZE, Double.POSITIVE_INFINITY, Priority.ALWAYS, VPos.CENTER, true));
        }
        initClient(root); /* Initializes the client for our game */

        /* fetch all participants through the client api */
        for (BattleParticipant participant : c.getAllPlayers()) {
            Circle circle = new Circle();
            String color;
            if (participant.getPlayerId()==c.player.getPlayerId()) color="blue";
            else if (String.valueOf(participant.getPlayerId()).contains("11111")) color="red";
            else color="grey";
            circle.setFill(Paint.valueOf(color));
            circle.setRadius(BATTLEFIELDSIZE <= 25 ? 8 : 3);
            participants.put(participant.getPlayerId(),tuple(circle,participant));
            root.add(circle, participant.getXPos(), (size-1)-participant.getYPos());
            circle.setOnContextMenuRequested(getMenuHandler(circle,participant));
            setTooltip(participant,circle);
        }

        Scene scene = new Scene(root, 1024, 1024);
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                Circle circle = participants.get(c.player.getPlayerId()).a;
                switch (event.getCode()) {
                    case W: c.performAction(Action.UP); break;
                    case S: c.performAction(Action.DOWN); break;
                    case A: c.performAction(Action.LEFT); break;
                    case D: c.performAction(Action.RIGHT); break;
                    case UP:
                    case DOWN:
                    case RIGHT:
                    case LEFT:
                        c.performAction(Action.valueOf(event.getCode().toString()));
                        break;
                }
            }
        });
        primaryStage.setTitle("DAS GAME");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void setTooltip(BattleParticipant p, Node n) {
        Tooltip tooltip = new Tooltip();
        tooltip.setText(String.format("id:%s\nhp:%s,ap:%s",p.getPlayerId(),p.getHp(),p.getAp()));
        Tooltip.install(n,tooltip);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
