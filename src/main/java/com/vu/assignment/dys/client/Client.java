package com.vu.assignment.dys.client;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;

import com.hazelcast.core.*;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;
import com.hazelcast.map.listener.MapListener;
import com.hazelcast.monitor.LocalMapStats;
import com.vu.assignment.dys.domain.*;

import static com.vu.assignment.dys.shared.Utility.*;
import static com.vu.assignment.dys.domain.Action.*;
import static com.vu.assignment.dys.domain.Participant.BATTLEFIELDSIZE;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Client {
    private List<IActionListener> listeners = new ArrayList<>();
    StatsListener statisticListener;
    static String publicAddress = "192.168.178.20";
    static int publicPort = 5701;
    HazelcastInstance hz;
    IQueue<String> tests;
    IQueue<Map<String, Map<String,Long>>> statistics;
    public void setStatisticListener(StatsListener statisticListener) {
        this.statisticListener = statisticListener;
        statistics = hz.getQueue("statistics");
        statistics.addItemListener(new ItemListener<Map<String, Map<String,Long>>>() {
            @Override
            public void itemAdded(ItemEvent<Map<String, Map<String,Long>>> itemEvent) {
                System.out.println("statistics added");
                statisticListener.onStatisticsUpdate(itemEvent.getItem());
            }

            @Override
            public void itemRemoved(ItemEvent<Map<String, Map<String,Long>>> itemEvent) {}
        },true);
    }
    BattleParticipant player;
    /**             player
     *              /   \
     *     playerId   /  \ ____
     *              /\   / \   \
     *             x  y  hp ap  initial hp
     */
    IMap<Integer, Tuple<Tuple<Integer,Integer>,Tuple3<Integer,Integer,Integer>>> players;
    MapListener entryAdded = new EntryAddedListener<Integer,Tuple<Tuple<Integer,Integer>,Tuple3<Integer,Integer,Integer>>>() {
        @Override
        public void entryAdded(EntryEvent<Integer, Tuple<Tuple<Integer, Integer>, Tuple3<Integer, Integer,Integer>>> entryEvent) {
            if (entryEvent.getKey() != player.getPlayerId()) {
                BattleParticipant participant = playerFromTuple(entryEvent);
                System.out.println(String.format("player added at coords (%s,%s).", participant.getXPos(), participant.getYPos()));
                if (String.valueOf(participant.getPlayerId()).contains("11111"))
                    getListeners().forEach(s -> { s.perform(Action.SPAWNDRAGON, participant); });
                else getListeners().forEach(s -> { s.perform(Action.CONNECT, participant); });
            }
        }
    };
    MapListener entryRemoved = new EntryRemovedListener<Integer,Tuple<Tuple<Integer,Integer>,Tuple3<Integer,Integer,Integer>>>() {
        @Override
        public void entryRemoved(EntryEvent<Integer,Tuple<Tuple<Integer,Integer>,Tuple3<Integer,Integer,Integer>>> entryEvent) {
            System.out.println(String.format("Player removed %s",entryEvent.getKey()));
            getListeners().forEach(s -> s.perform(Action.DISCONNECT, new Player(entryEvent.getKey())));
        }
    };
    MapListener entryUpdated = new EntryUpdatedListener<Integer,Tuple<Tuple<Integer,Integer>,Tuple3<Integer,Integer,Integer>>>() {
        @Override
        public void entryUpdated(EntryEvent<Integer, Tuple<Tuple<Integer, Integer>, Tuple3<Integer, Integer,Integer>>> entryEvent) {
            BattleParticipant updatedParticipant = playerFromTuple(entryEvent);
//            System.out.println("Player updated");
//            System.out.println(updatedParticipant);
            getListeners().forEach(s -> s.perform(Action.UPDATE, updatedParticipant));
        }
    };

    public Map<Integer, BattleParticipant> getAllPlayersMap() {
       return players.entrySet().stream().collect(Collectors.toMap(k -> k.getKey(), v-> playerFromTuple(0,v.getValue())));
    }

    public Set<BattleParticipant> getAllPlayers() {
        return players.entrySet().stream().map(s-> playerFromTuple(s.getKey(),s.getValue())).collect(Collectors.toSet());
    }

    private static BattleParticipant playerFromTuple(EntryEvent<Integer, Tuple<Tuple<Integer, Integer>, Tuple3<Integer, Integer,Integer>>> ee) {
        Tuple<Tuple<Integer, Integer>, Tuple3<Integer, Integer,Integer>> value = ee.getValue();
        return new Player(ee.getKey(), value.a.a,value.a.b,value.b.a,value.b.b,value.b.c);
    }

    static BattleParticipant playerFromTuple(int id, Tuple<Tuple<Integer, Integer>, Tuple3<Integer, Integer,Integer>> data) {
        return new Player(id, data.a.a,data.a.b,data.b.a,data.b.b,data.b.c);
    }

    public LocalMapStats getMapStatistics() {
        return players.getLocalMapStats();
    }

    public List<IActionListener> getListeners() { return listeners; }
    public void addListener(IActionListener listener) { listeners.add(listener); }

    public Client(boolean isPlayer) {
        player = isPlayer ? new Player() : new Dragon();
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getGroupConfig().setName("server-cluster").setPassword("dyssystems");
        clientConfig.getNetworkConfig().addAddress(publicAddress); /* default address, using default port */
        hz = HazelcastClient.newHazelcastClient(clientConfig);
        players = hz.getMap("players");
        tests = hz.getQueue("tests");
        players.addEntryListener(entryAdded,true);
        players.addEntryListener(entryRemoved,true);
        players.addEntryListener(entryUpdated,true);
    }

    private Tuple tupleFromPlayer(BattleParticipant p) {
        return tuple(tuple2(p.getXPos(),p.getYPos()),tuple3(p.getHp(),p.getAp(),p.getInitialHp()));
    }

    int getDistance(BattleParticipant p1, BattleParticipant p2) {
        int nx = Math.max(p1.getXPos(),p2.getXPos())-Math.min(p1.getXPos(),p2.getXPos()); // 5
        int ny = Math.max(p1.getYPos(),p2.getYPos())-Math.min(p1.getYPos(),p2.getYPos()); // 2
        return (nx+ny);
    }

    void performAction(Action action, int otherPlayerId) {
        if (action.equals(Action.GETSTATS)) { tests.offer("getstats"); return; }
        if (!action.equals(CONNECT)) if (!players.containsKey(player.getPlayerId())) {
            System.out.println("You are not connected."); return;
        }
        switch (action) {
            case CONNECT:
                System.out.println("Connecting");
                if (players.put(this.player.getPlayerId(), tupleFromPlayer(this.player)) == null)
                    System.out.println("Player added");
                break;
            case DISCONNECT:
                System.out.println("Disconnecting");
                if (players.remove(this.player.getPlayerId()) != null)
                    System.out.println("Player removed");
                break;
            case UP:
            case DOWN:
            case LEFT:
            case RIGHT:
                Tuple2 direction = direction(action, player);
                if (direction.a==BATTLEFIELDSIZE||direction.b==BATTLEFIELDSIZE
                        || direction.a < 0 || direction.b < 0)
                    { System.out.println("coords out of bounds"); return; }
                /* check if user coords are preoccupied*/
                if (players.values().stream().anyMatch(s -> s.a.toString().equals(direction.toString()))) {
                    System.out.println("Coordinates already preoccupied"); return;
                } else {    /* Update the user coordinates */
                    player.setXPos(direction.a); player.setYPos(direction.b);
                    players.put(player.getPlayerId(),tupleFromPlayer(player));
                }
                break;
            case HEAL:
                if (players.containsKey(otherPlayerId)) {
                    BattleParticipant otherPlayer = playerFromTuple(otherPlayerId,players.get(otherPlayerId));
                    if(getDistance(player,otherPlayer) <= 5) {
                        int newHp = otherPlayer.getHp() + player.getAp();
                        otherPlayer.setHp(newHp <= otherPlayer.getInitialHp() ? newHp : otherPlayer.getInitialHp());
                        if (otherPlayer.getHp() <= otherPlayer.getInitialHp())
                            players.put(otherPlayerId,tupleFromPlayer(otherPlayer));
                    }
                }
                break;
            case STRIKE:
                if (players.containsKey(otherPlayerId)) {
                    BattleParticipant otherPlayer = playerFromTuple(otherPlayerId,players.get(otherPlayerId));
                    if (getDistance(player,otherPlayer) <= 2) {
                        otherPlayer.setHp(otherPlayer.getHp() - player.getAp());
                        if (otherPlayer.getHp() <= 0) {
                            System.out.println("going to remove");
                            players.remove(otherPlayerId);
                        }
                        else players.put(otherPlayerId, tupleFromPlayer(otherPlayer));
                    }
                }
                break;
            case SHUTDOWNNODE:
                tests.offer("shutdownnode");
                break;
            case ADDNODE:
                tests.offer("addnode");
                break;
            case GETSTATS:
                tests.offer("getstats");
                break;
        }
    }

    void performAction(Action action) {
        performAction(action,0);
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            publicAddress=args[0];
            publicPort=Integer.valueOf(args[1]);
            Participant.BATTLEFIELDSIZE=Integer.valueOf(args[2]);
        }
        Client c = new Client(true);
        System.out.println("Player id : " + c.player.getPlayerId());
        c.performAction(CONNECT);

        while (true) {
            Scanner scanner = new Scanner(System.in);
            Pattern p = Pattern.compile("(HEAL|STRIKE)\\s([0-9]*)", Pattern.CASE_INSENSITIVE);
            String cmd = scanner.findInLine(p);
            if (cmd==null) {
                Action userAction;
                try {
                    String next = scanner.next();
                    System.out.println(next.toUpperCase());
                    userAction = Action.valueOf(next.toUpperCase());
                } catch (IllegalArgumentException iae) {
                    System.out.println(String.format("Illegal argument! The following operations are supported:\n%s",
                            Arrays.toString(Action.values()))); continue;
                }
                c.performAction(userAction);
            } else {
                String[] split = cmd.split("\\s");
                if (split.length==2) c.performAction(Action.valueOf(split[0].toUpperCase()),Integer.valueOf(split[1]));
            }
        }
    }
}