package com.vu.assignment.dys.client;

import com.vu.assignment.dys.cluster.Cluster;
import com.vu.assignment.dys.domain.Action;
import com.vu.assignment.dys.domain.BattleParticipant;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class PerformanceMeasureClient {
    public static Logger logger = Logger.getLogger(Cluster.class.getName());
    public static FileHandler fh;


    public static void main(String[] args) {
        if (args.length > 0) {
            Client.publicAddress=args[0];
            Client.publicPort=Integer.valueOf(args[1]);
        }
        try {
            fh = new FileHandler(System.getProperty("user.dir") + "/clusterstatistics.log");
            logger.addHandler(fh);
            fh.setFormatter(new SimpleFormatter());
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        Client c = new Client(true);
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                c.performAction(Action.GETSTATS);
            }
        }, 3, 3, TimeUnit.SECONDS);

        c.setStatisticListener(new StatsListener() {

            @Override
            public void perform(Action action, BattleParticipant participant) { }

            @Override
            public void onStatisticsUpdate(Map<String, Map<String,Long>> item) {
                StringBuilder statisticsInfo = new StringBuilder();
                statisticsInfo.append("\n|------------------------------------------|\n");

                for (Map.Entry<String, Map<String, Long>> next : item.entrySet()) {

                    statisticsInfo.append(String.format("|%-42s|\n","Statistics for Node:") +
                                          String.format("|%-42s|\n", next.getKey()) +
                                          "|------------------------------------------|\n");

                    for(Map.Entry<String, Long> aNode: next.getValue().entrySet())
                        statisticsInfo.append(String.format("|%-30s: %10s|\n" + "",aNode.getKey(),aNode.getValue()));

                    statisticsInfo.append("+------------------------------------------+\n");

                }

                statisticsInfo.append("\n\n\n");

                logger.info(statisticsInfo.toString());

            }
        });
    }
}
