package com.vu.assignment.dys.client;

import com.vu.assignment.dys.domain.Action;
import com.vu.assignment.dys.domain.BattleParticipant;
import com.vu.assignment.dys.domain.Participant;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.vu.assignment.dys.domain.Action.*;

public class DragonBot {
    public static void main(String[] args) {
        int amountOfDragons=100;
        if (args.length > 0) {
            Client.publicAddress=args[0];
            Client.publicPort=Integer.valueOf(args[1]);
            Participant.BATTLEFIELDSIZE = Integer.valueOf(args[2]);
            amountOfDragons = Integer.valueOf(args[3]);
        }
        for (int i =0; i<amountOfDragons;i++) {
            Map<Integer,BattleParticipant> participants = new HashMap<>();
            Client c = new Client(false);
            c.performAction(CONNECT);
            participants.putAll(c.getAllPlayersMap());

            c.addListener(new IActionListener() {
                @Override
                public void perform(Action action, BattleParticipant participant) {
                    switch (action) {
                        case CONNECT:
                            participants.put(participant.getPlayerId(),participant);
                            break;
                        case UPDATE:
                            participants.put(participant.getPlayerId(),participant);
                            break;
                        case DISCONNECT:
                            participants.remove(participant.getPlayerId());
                            break;
                        default: break;
                    }
                }
            });

            Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    for (Map.Entry<Integer,BattleParticipant> entry : participants.entrySet()) {
                        if(entry.getKey() != c.player.getPlayerId()
                                && !String.valueOf(entry.getKey()).startsWith("11111")) {
                            int distance = c.getDistance(c.player, entry.getValue());
                            if (distance <= 2) {
                                c.performAction(STRIKE, entry.getKey()); break;
                            }
                        }
                    }
                }
            }, 2, 2, TimeUnit.SECONDS);
        }
    }
}
